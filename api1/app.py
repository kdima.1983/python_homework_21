from flask import Flask
import requests

app = Flask(__name__)

@app.route("/")
def home():
    r = "1"
    try:
        response = requests.get("http://api2", params={})
        if response.status_code == 200:
            r = r + "=>" + response.text
    finally:
        return r

app.run()